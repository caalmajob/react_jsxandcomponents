// 1. Librería de React
// 2. Librerías de terceros
// 3. Mis propios componentes, además siguiendo (si puedo) el orden de uso de estos componentes.
// 4. Estilos

import React, { Component } from 'react';
import Navbar from './components/Navbar';
import Header from './components/Header';
import Section from './components/Section';
import './App.scss';
class App extends Component {

  state = {
    isEditing: false,
    user: {
      firstName: 'Juan',
      lastName: 'González',
      age: 30,
      image: 'https://bit.ly/3e7XhiJ',
      about: 'Soy una persona que siempre está aprendiendo cosas nuevas',
      education: [
        { title: 'Ingeniero civil', company: 'Universidad de Salamanca' },
        { title: 'Máster en Big Data', company: 'Universidad de Madrid' },
      ],
      experience: [
        { title: 'Junior data scientist', company: 'Upgrade Hub' },
        { title: 'Senior data scientist', company: 'Upgrade Hub Labs' },
      ],
    },
  }

  setIsEditing = () => {
    this.setState({ isEditing: !this.state.isEditing });
  }

  addItem = (name, title, company) => {
    let newUser = this.state.user;

    newUser[name].push({title, company});

    this.setState({user: newUser})
  };

  submitEditProfile = () => {
    this.isEditing();
  }
  
  handleChangeName = (ev) => {
    const newUser = this.state.user;
    newUser.firstName = ev.target.value;

    this.setState({ user: newUser });
  }

  render() {
    const { user } = this.state;

    return (
      <div className="App">
        {this.state.isEditing
        ? (<form onSubmit={this.submitEditProfile}>
          <label>
            <p>Nombre de usuario</p>
            <input type="text" name="user" value={user.firstName} onChange={this.handleChangeName}/>
          </label>
        </form>)
        : (
          <>
            <Navbar addMethod={this.addMethod} addArrow={this.addArrow} test={'Prueba!!'}/>
            <Header
              userImage={user.image}
              fullName={`${user.firstName} ${user.lastName}`}
              test={"Hola"}
            />
            <Section elements={user.education} name={'education'} addItem={this.addItem} />
            <Section elements={user.experience} name={'experience'} addItem={this.addItem} />
          </>
        )}

        <button className="edit-profile" onClick={this.setIsEditing}>Editar Perfil</button>
      </div>
    );
  }
}
export default App;
