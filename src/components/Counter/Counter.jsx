import React, { Component } from 'react';

class Counter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            counter: 0,
        }
    }
    add = () => {
        this.setState({
            counter: this.state.counter + 1,
        })
    }

    substract = () => {
        this.setState({
            counter: this.state.counter - 1,
        })
    }
    render() {
        console.log(this.props);
        return (
            <div>
                <div className="counter">Counter: {this.state.counter}</div>
                <button onClick={this.add}>Sumar</button>
                <button onClick={this.substract}>Restar</button>
            </div>
        )
    }
}

export default Counter;


