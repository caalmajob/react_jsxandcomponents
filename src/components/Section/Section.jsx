import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Section.scss'
class Section extends Component {
    //Prop por defecto
    static defaultProp = {
        elements: [],
    };

    state = {
        openForm: false,
        title: '',
        company: '',
    };

    setOpenForm = () => {
        this.setState({ openForm: !this.state.openForm })
    };

    onInputChange = (ev) => {
        const { name, value } = ev.target;
        console.log(value);
        this.setState({ [name]: value });
    }

    submitForm = (ev) => {
        ev.preventDefault();

        const { title, company } = this.state;

        this.props.addItem(this.props.name, title, company)
        this.setOpenForm();
        this.setState({
            title: '',
            company: '',
        })
    }


    render() {
        return (
            <section className="section">
                <h2>{this.props.name}</h2>
                <ul className="flex-list">
                    {this.props.elements.map((el, index) => {
                        return (
                            <li key={`${el.title}/${el.company}-${index}`}>
                                <p>Title: {el.title}</p>
                                <p>Company: {el.company}</p>
                            </li>
                        );
                    })}
                </ul>
                {this.state.openForm && <div>
                    <form onSubmit={this.submitForm}>
                        <label htmlFor="title">Title</label>
                        <input
                            type="text"
                            name="title"
                            id="title"
                            placeholder="Title"
                            value={this.state.title}
                            onChange={this.onInputChange}
                        />

                        <label htmlFor="company">Company</label>
                        <input
                            type="text" 
                            name="company"
                            id="company"
                            placeholder="Company"
                            value={this.state.company}
                            onChange={this.onInputChange}
                        />

                        <button type="submit">Guardar</button>
                    </form>
                </div>}
                <button onClick={this.setOpenForm} >Añadir {this.props.name}</button>
            </section>
        );
    }
}

Section.propTypes = {
    elements: PropTypes.arrayOf(PropTypes.shape({
        title: PropTypes.string,
        company: PropTypes.string,
    })).isRequired
};

export default Section;
