import React, { Component } from 'react';
import './Header.scss';

class Header extends Component {

    render() {
        console.log(this.props);
        return (
            <header>
                <img className="user-image" src={this.props.userImage} alt="user image" />
                <h1>{this.props.fullName}</h1>
            </header>
        )
    }
}

export default Header;