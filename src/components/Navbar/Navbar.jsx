import React, { Component } from 'react';
import './Navbar.scss';

const language = 'es';
const spanishTitle = 'Bienvenid@!';
const englishTitle = 'Welcome!';

const navbarLinks = [ 'Home', 'About', 'Account']

class Navbar extends Component {

    render() {
        return (
            <nav className="nav">
                <h1>{language === 'es' ? spanishTitle : englishTitle}</h1>
                <ul className="flex-list">
                    {navbarLinks.map((item, index) => <li key={`${item}-${index}`}>{item}</li>
                    )}
                </ul>
            </nav>
        )
    }
}

export default Navbar;